from machine import Pin # Imports the machine library for the Pico
import time # Imports the (u)time library to add delay for the code. (converts to utime automatically)

def global_variables(): # Defines the variables as globals - making it callable inside loops.
    global led1 # Makes the function global (callable within loops)
    led1 = machine.Pin(14, machine.Pin.OUT) # Assigns GPIO 14 (pin 19 on the pico) to led1
    global led2 # Makes the function global (callable within loops)
    led2 = machine.Pin(15, machine.Pin.OUT) # Assigns GPIO 15 (pin 20 on the pico) to led2
    global btn1 # Makes the function global (callable within loops)
    btn1 = machine.Pin(18, machine.Pin.IN, Pin.PULL_UP) # Assigns GPIO 18 (24 on the pico) to btn1
    global btn2 # Makes the function global (callable within loops)
    btn2 = machine.Pin(19, machine.Pin.IN, Pin.PULL_UP) # Assigns GPIO 19 (25 on the pico) to btn1
    global led_onboard # Makes the function global (callable within loops)
    led_onboard = machine.Pin(25, machine.Pin.OUT) # Onboard LED is initialized with GPIO25
    global buzzer
    buzzer = machine.Pin(20, machine.Pin.OUT)
    

def led_onboard_def(): # Defines the onboard led behaviour
    led_onboard.toggle() # Changes the state of the onboard LED
    time.sleep(0.1) # Adds a delay for 0.1 seconds.
    led_onboard.toggle() # Changes the state of the onboard LED
    time.sleep(0.1) # Adds a delay for 0.1 seconds.

def led1_def(): # Defines the led1 bahaviour
    led1.toggle() # Changes the state of led1
    time.sleep(0.1) # Adds a delay for 0.1 seconds.
    led1.toggle() # Changes the state of led1

def led2_def(): # Defines the led2 behaviour
    led2.toggle() # Changes the state of led2
    time.sleep(0.1) # Adds a delay for 0.1 seconds.
    led2.toggle() # Changes the state of led2
    
def buzzer_def(): # Defines the buzzer behaviour
    buzzer.value(0) #Turns the buzzer on


while True: #Main loop of the program
    global_variables() #Calls the variables from the function global_variables.
    if btn1.value() == 0: #senses if the button has been pressed. 
        buzzer_def()
        led1_def() # Calls the led1_def function and runs the indented code.
        led_onboard_def() # Calles the led_onboard_def() function and runs the indented code.
    elif btn2.value() == 0: #senses if the button has been pressed.
        buzzer_def()
        led2_def() # Calles the led2_def function and runs the indented code.
        led_onboard_def() # Calles the led_onboard_def() function and runs the indented code.
        led_onboard_def() # Calles the led_onboard_def() function and runs the indented code.
    else: # If nothing is pressed it runs the following lines of code
        led1.value(0) # Turns led1 off
        led2.value(0) # Turns led2 off
        led_onboard.value(0) # Turns onboard led off
        buzzer.value(1) # Turns the buzzer off
 
