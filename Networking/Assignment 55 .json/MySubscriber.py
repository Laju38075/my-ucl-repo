import paho.mqtt.client as mqtt
brokerIP ="192.168.87.81"
brokerPort = 1883
brokerKeepAlive = 60
myTopic = "testTopic"

def on_connect(client,userdata,flags,rc):
        client.subscribe(myTopic)

def message(client,userdata,msg):
        print("Fetched: " + str(msg.payload.decode()) + " from topic " + myTopic)
        client.disconnect()

client = mqtt.Client()
client.connect(brokerIP, brokerPort, brokerKeepAlive)
client.on_connect = on_connect
client.on_message = message
client.loop_forever()
