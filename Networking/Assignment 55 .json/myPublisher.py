import paho.mqtt.client as mqtt
import random

brokerIP = "192.168.87.81"
brokerPort = 1883
brokerKeepAlive = 60

myTopic = "testTopic"
myPayload = random.randint(0,99)
myQoS = 1
myRetain = True

client = mqtt.Client()
client.connect(brokerIP,brokerPort,brokerKeepAlive)
client.publish(topic=myTopic,qos =myQoS,payload=myPayload,retain=myRetain);

print(str(myPayload) + " has been published on broker " + brokerIP + ":" + str(brokerPort) + " on topic: " + myTopic)

client.disconnect();
