## Computer Vision Portfolio Template

Author name: Lasse Justesen
Education: UCL - IT Technology
- [Computer Vision Portfolio Template](#computer-vision-portfolio-template)
- [Introduction](#introduction)
- [Methodology and process](#methodology-and-process)
	- [Use case 1](#use-case-1)
	- [Use case 2](#use-case-2)
	- [Use case 3](#use-case-3)
- [Results and evaluation](#results-and-evaluation)
	- [Use case 1](#use-case-1-1)
	- [Use case 2](#use-case-2-1)
- [Appendix](#appendix)
		- [Use case 1](#use-case-1-2)
			- [Appendix1](#appendix1)
			- [Appendix2](#appendix2)


## Introduction

## Methodology and process

### Use case 1

I wanted to create a program that would count the number of eyes on a dice in an image. Firstly I converted the color to grayscale using cvtColor, I then used Gaussian blur to reduce the noice in the image, to avoid false positives. I then used HughCircles to create a new array of the circles, and then used the Circles function to map the circles onto the original image. 
For this usecase I also attempted with another form of detection algorithm, which is the SimpleBlobDetection.



### Use case 2

The second use case is a continuation of the first use case. For this usecase, I would like to update the first use case in order for it to be able to work on a video of a dice updating in real time, as well as calculated the amount of circles of each dice roll.


### Use case 3

## Results and evaluation

### Use case 1
Firstly I converted the color to grayscale using cvtColor, I then used Gaussian blur to reduce the noice in the image, to avoid false positives.

Before blurring:
![Imgur](https://i.imgur.com/Da5ulBV.png)

After blurring with a 9 by 9 kernel and converting to grayscale: 
(Keep in mind the original image is already in grayscale)

![Imgur](https://i.imgur.com/EhQpAeP.png)


Now that we have reduced the noise in the image, we can apply the Hough transform(HughCircles) to find the circles in the image.

![Imgur](https://i.imgur.com/OF4Ma4D.png)

Even though it raised a lot of errors - The HughCircles can take an image and calculate the circles based off of different  parameters. The main parameres to be altered would be minDist, Param1 and Param2. I found that the specific values for my dice is:

	HoughCircles(gray, circles, HOUGH_GRADIENT,1, 10, 200, 50, 0, 0);

These would be subject to change if the diameter of the circles would change. Further documentation regarding the HughCircles is available here - 

[OpenCV Documentation](https://docs.opencv.org/4.x/dd/d1a/group__imgproc__feature.html#ga47849c3be0d0406ad3ca45db65a25d2d)

Since the HoughCircles also creates an output array called "circles" we can use a for-loop to iterate through the array and draw all the circles onto the orignal image.

![Imgur](https://i.imgur.com/BbNgZVi.png)

For further testing with the HoughCircles function, I've included more images in the appendix.

In order to test different algorithms to find the number of eyes on a dice, I've also used the simpleBlobDetector which yieled the following result on the orignal 1st. dice:
![Imgur](https://i.imgur.com/LEGGUSu.png)
which was also able to find the eyes on the dice, with the following parameters set in the SimpleBlobDetector:

	params.minThreshold = 0;
	params.maxThreshold = 255;
	params.filterByArea = false;
	params.filterByCircularity = false;
	params.filterByConvexity = true;
	params.minConvexity = 0.1;
	params.filterByInertia = false;

Additionally, the SimpleBlobDetector was also able to find all of the eyes on the image where the HoughCircles failed:
![Imgur](https://i.imgur.com/8XkdtBb.png)

The only change here has been made to the following variables:

	params.filterByArea = true;
	params.minArea = 25;
	params.filterByConvexity = false;
### Use case 2


![[Imgur]((https://youtu.be/8tf-8WBfKGo)](https://i.imgur.com/srMzOyr.png)
.....

## Appendix

#### Use case 1
Testing with a different roll of the same dice:
##### Appendix1
![Imgur](https://i.imgur.com/gmgsJQh.png)

Testing with a different roll of a different dice:
##### Appendix2
![Imgur](https://i.imgur.com/yqvQmnG.png)
As seen here, the algorithm doesn't succesfully find all the eyes on the dice. This could potentially be fixed by tweaking the values in the function. Furthermore, the image of the "failed" dice counter is from another perspective, with additional features which could mean that the threshhold value would have to be changed. 