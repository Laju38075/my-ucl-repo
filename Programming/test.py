import PySimpleGUI as sg

amount = 0

def iron_ingot():
    print('Iron function called')
    global amount
    Ironore = amount * 4
    Charcoal = amount * 2
    text = 'You would need ',Ironore,' Iron ore, and:', Charcoal, 'Wood for Charcoal'
    #print(text) Prints correct to terminal but doesn't bring it back when function called.
    return text

#First Frame
layout_frame1 = [
    [sg.Text("What are you doing?")],
    [sg.Button('Smelting')],
    [sg.Button('Stonecutting')],
    [sg.Button('Tanning')],
    [sg.Button('Weaving')],
    [sg.Button('Woodworking')],      
]
### SMELTING LAYOUTS ###
layout_smelting = [
    [sg.Text("What are you smelting?")],
    [sg.Button('Iron Ingots')],
    [sg.Button('Steel Ingots')],
    [sg.Button('Starmetal Ingots')],
    [sg.Button('Orichalcum Ingots')],
    [sg.Button('Asmodeum Ingots')],      
]
layout_ironingots = [
    [sg.Text("how many are you crafing?")],
    [sg.Input(key='iron_amount')],
    [sg.Button("OK")]
]

layout_result = [
    [sg.Text(iron_ingot())]
]

"""
HOW TO CONVERT THE DATA FROM THE FUNCTION INTO SG.TEXT????
"""
    
window = sg.Window('Title',layout_frame1)
while True:
    event, values = window.read()
    if event == 'Exit' or event == sg.WIN_CLOSED:
        break
    if event =="Smelting":
        window.close()
        window = sg.Window("Smelting",layout_smelting, margins=(50,40))
    if event =="Iron Ingots":
        window.close()
        window = sg.Window("Iron Ingots",layout_ironingots, margins=(50,40))
    if event == 'OK':
        try:
            amount = int(values['iron_amount'])
        except ValueError:
            print('Please only enter numbers')
            quit()
        window.close()
        iron_ingot()
        window = sg.Window("layout_result",layout_result, margins=(50,40))
        
        



window.close()