import PySimpleGUI as sg

def basemat4(amount):
    return str(amount*4)

def basemat2(amount):
    return str(amount*2)
layout_frame1 = [
    [sg.Button('Smelting',size=(15,1))],
    [sg.Button('Stonecutting',size=(15,1))],
    [sg.Button('Tanning',size=(15,1))],
    [sg.Button('Weaving',size=(15,1))],
    [sg.Button('Woodworking',size=(15,1))],      
]
radio1 = [
    [sg.Radio('Iron ore',"RADIO1", default=False,key='iron_radio')],
    [sg.Radio('Steel ingots',"RADIO1", default=False,)],
    [sg.Radio('Starmetal Ingots',"RADIO1", default=False)],
    [sg.Radio('Orichalcum Ingots',"RADIO1", default=False)],
    [sg.Radio('Asmodeum Ingots',"RADIO1", default=False)]
]
radio2 = [
    [sg.Radio('Stone Blocks',"RADIO1", default=False,key='stone_block_radio')],
    [sg.Radio('Stone Bricks',"RADIO1", default=False)],
    [sg.Radio('Lodestone Bricks',"RADIO1", default=False)],
    [sg.Radio('Obsidian Voidstone',"RADIO1", default=False)],
    [sg.Radio('Runestone',"RADIO1", default=False)]
]
radio3 = [
    [sg.Radio('Coarse Leather',"RADIO1", default=False,key='coarse_leather_radio')],
    [sg.Radio('Rugged Leather',"RADIO1", default=False)],
    [sg.Radio('Layered Leather',"RADIO1", default=False)],
    [sg.Radio('Infused Leather',"RADIO1", default=False)],
    [sg.Radio('Runic Leather',"RADIO1", default=False)]
]
radio4 = [
    [sg.Radio('Linen',"RADIO1", default=False,key="linen_radio")],
    [sg.Radio('Sateen',"RADIO1", default=False)],
    [sg.Radio('Silk',"RADIO1", default=False)],
    [sg.Radio('Infused Silk',"RADIO1", default=False)],
    [sg.Radio('Pheonixweave',"RADIO1", default=False)]
]
radio5 = [
    [sg.Radio('Timber',"RADIO1", default=False,key='timber_radio')],
    [sg.Radio('Lumber',"RADIO1", default=False)],
    [sg.Radio('Wyrdwood Planks',"RADIO1", default=False)],
    [sg.Radio('Ironwood Planks',"RADIO1", default=False)],
    [sg.Radio('Glittering Ebony',"RADIO1", default=False)]
]
layout_result = [
    [sg.Text('How many are you crafting?',)],
    [sg.Input(key='-input-')],
    [sg.Button("Calculate")],
    [sg.Text("You would need...",key='-output-')]
]
def radio_layout():
    return sg.Frame("",     [
                            [sg.Column(radio1,key='smelting',visible=True),
                            sg.Column(radio2,key='stonecutting',visible=False),
                            sg.Column(radio3,key='tanning',visible=False),
                            sg.Column(radio4,key='weaving',visible=False),
                            sg.Column(radio5,key='woodworking',visible=False)]], 
                            pad=(5, 3), expand_x=True, expand_y=True, border_width=0)

main_frame = [
    [sg.Frame("Frame1", layout_frame1,), radio_layout()],
    [sg.Frame("Frame3", layout_result, )]
]

window = sg.Window('Calculator',main_frame,)
while True:
    event, values = window.read()
    if event == 'Exit' or event == sg.WIN_CLOSED:
        break
    if event == "Smelting":
        window[f'smelting'].update(visible=True)
        window[f'stonecutting'].update(visible=False)
        window[f'tanning'].update(visible=False)
        window[f'weaving'].update(visible=False)
        window[f'woodworking'].update(visible=False)
    if event == "Stonecutting":
        window[f'smelting'].update(visible=False)
        window[f'stonecutting'].update(visible=True)
        window[f'tanning'].update(visible=False)
        window[f'weaving'].update(visible=False)
        window[f'woodworking'].update(visible=False)
    if event == "Tanning":
        window[f'smelting'].update(visible=False)
        window[f'stonecutting'].update(visible=False)
        window[f'tanning'].update(visible=True)
        window[f'weaving'].update(visible=False)
        window[f'woodworking'].update(visible=False)
    if event == "Weaving":
        window[f'smelting'].update(visible=False)
        window[f'stonecutting'].update(visible=False)
        window[f'tanning'].update(visible=False)
        window[f'weaving'].update(visible=True)
        window[f'woodworking'].update(visible=False)
    if event == "Woodworking":
        window[f'smelting'].update(visible=False)
        window[f'stonecutting'].update(visible=False)
        window[f'tanning'].update(visible=False)
        window[f'weaving'].update(visible=False)
        window[f'woodworking'].update(visible=True)
    if event ==  "Calculate":
        try: 
            amount =int(values['-input-'])
            if values['iron_radio'] == True:
                text = 'You would need ' + basemat4(amount) + ' Iron ore, and '+ basemat2(amount)+ ' wood for Charcoal'
            if values['stone_block_radio'] == True:
                text ='You would need ' + basemat4(amount) + ' stone'
            if values['coarse_leather_radio'] == True:
                text ='You would need ' + basemat4(amount) + ' rawhide'
            if values['linen_radio'] == True:
                text ='You would need ' + basemat4(amount) +  ' fiber'
            if values['timber_radio'] == True:
                text ='You would need ' + basemat4(amount) + ' green wood'
        except ValueError:
            print('Please only input numbers')
            text = "Please only input numbers"

        window['-output-'].update(text)

window.close()