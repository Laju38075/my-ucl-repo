import PySimpleGUI as sg
#First Frame
layout_frame1 = [
    [sg.Text("What are you doing?")],
    [sg.Button('Smelting')],
    [sg.Button('Stonecutting')],
    [sg.Button('Tanning')],
    [sg.Button('Weaving')],
    [sg.Button('Woodworking')],      
]
### SMELTING LAYOUTS ###
layout_smelting = [
    [sg.Text("What are you smelting?")],
    [sg.Button('Iron Ingots')],
    [sg.Button('Steel Ingots')],
    [sg.Button('Starmetal Ingots')],
    [sg.Button('Orichalcum Ingots')],
    [sg.Button('Asmodeum Ingots')],      
]
layout_ironingots = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]
]
layout_steelingots = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]
]
layout_starmetalingots = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]
]
layout_orichalcumingots = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]
]
layout_asmodeumingots = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]
]
### SMELTING LAYOUTS ###
'''
'''
### STONECUTTING LAYOUTS ###
layout_stonecutting = [
    [sg.Text("What are you cutting?")],
    [sg.Button('Stone Blocks')],
    [sg.Button('Stone Bricks')],
    [sg.Button('Lodestone Bricks')],
    [sg.Button('Obsidian Voidstone')],
    [sg.Button('Runestone')],      
]
layout_stoneblocks = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]
]
layout_stonebricks = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]
]
layout_lodestonebricks = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]
]
layout_obsidianvoidstone = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]
]
layout_runestone = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]
]
### STONECUTTING LAYOUTS ###
'''
'''
### TANNING LAYOUTS ###
layout_tanning = [
    [sg.Text("What are you tanning?")],
    [sg.Button('Coarse leather')],
    [sg.Button('Rugged Leather')],
    [sg.Button('Layered leather')],
    [sg.Button('Infused Leather')],
    [sg.Button('Runic Leather')],      
]
layout_coarseleather = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_ruggedleather = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_layeredleather = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_infusedleather = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_runicleather = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
### TANNING LAYOUTS ###
'''
'''
### WEAVING LAYOUTS ###
layout_weaving = [
    [sg.Text("What are you weaving?")],
    [sg.Button('Linen')],
    [sg.Button('Sateen')],
    [sg.Button('Silk')],
    [sg.Button('Infused Silk')],
    [sg.Button('Phoenixweave')],      
]
layout_linen = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_sateen = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_silk = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_infusedsilk = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_phoenixweave = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
### WEAVING LAYOUTS ###
'''
'''
### WOODWORKING LAYOUTS ###
layout_woodworking = [
    [sg.Text("What are you making?")],
    [sg.Button('Timber')],
    [sg.Button('Lumber')],
    [sg.Button('Wyrdwood Planks')],
    [sg.Button('Ironwood Planks')],
    [sg.Button('Glittering Ebony')],      
]
layout_timber = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_lumber = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_wyrdwoodplanks = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_ironwoodplanks = [
    [sg.Text("how many are you cratfing?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
layout_glitteringebony = [
    [sg.Text("how many are you crafting?")],
    [sg.InputText()],
    [sg.Text('Required amount goes here', key='-OUT-')],
    [sg.Button("OK")]     
]
### WOODWORKING LAYOUTS ###
'''
'''

#Window
sg.theme('BrownBlue')
window = sg.Window("Calculator",layout_frame1, margins=(50,40))
#Event loop
while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED:
        break
    ### Smelting Branch ###
    if event =="Smelting":
        window.close()
        window = sg.Window("Smelting",layout_smelting, margins=(50,40))
    if event =="Iron Ingots":
        window.close()
        window = sg.Window("Iron Ingots",layout_ironingots, margins=(50,40))
    if event =="Steel Ingots":
        window.close()
        window = sg.Window("Steel Ingots",layout_steelingots, margins=(50,40))
    if event =="Starmetal Ingots":
        window.close()
        window = sg.Window("Starmetal Ingots",layout_starmetalingots, margins=(50,40))
    if event =="Orichalcum Ingots":
        window.close()
        window = sg.Window("Orichalcum Ingots",layout_orichalcumingots, margins=(50,40))
    if event =="Asmodeum Ingots":
        window.close()
        window = sg.Window("Asmodeum Ingots",layout_asmodeumingots, margins=(50,40))
    ### Smelting Branch ###

    ### Stonecutting Branch ###
    if event =="Stonecutting":
        window.close()
        window = sg.Window("Stonecutting",layout_stonecutting, margins=(50,40))
    if event =="Stone Blocks":
        window.close()
        window = sg.Window("Stone Blocks",layout_stoneblocks, margins=(50,40))
    if event =="Stone Bricks":
        window.close()
        window = sg.Window("Stone Bricks",layout_stonebricks, margins=(50,40))   
    if event =="Lodestone Bricks":
        window.close()
        window = sg.Window("Lodestone Bricks",layout_lodestonebricks, margins=(50,40))  
    if event =="Obsidian Voidstone":
        window.close()
        window = sg.Window("Obsidian Voidstone",layout_obsidianvoidstone, margins=(50,40))   
    if event =="Runestone":
        window.close()
        window = sg.Window("Runestone",layout_runestone, margins=(50,40))        
    ### Stonecutting Branch ###

    ### Tanning Branch ###
    if event =="Tanning":
        window.close()
        window = sg.Window("Tanning",layout_tanning, margins=(50,40))
    if event =="Coarse Leather":
        window.close()
        window = sg.Window("Coarse Leather",layout_coarseleather, margins=(50,40))
    if event =="Rugged Leather":
        window.close()
        window = sg.Window("Rugged Leather",layout_ruggedleather, margins=(50,40))     
    if event =="Layered Leather":
        window.close()
        window = sg.Window("Layered Leather",layout_layeredleather, margins=(50,40))
    if event =="Infused Leather":
        window.close()
        window = sg.Window("Infused Leather",layout_infusedleather, margins=(50,40))
    if event =="Runic Leather":
        window.close()
        window = sg.Window("Runic Leather",layout_runicleather, margins=(50,40))
        ###Tanning Branch ###
    ### Tanning Branch ###

    ### Weaving Branch ###
    if event =="Weaving":
        window.close()
        window = sg.Window("Weaving",layout_weaving, margins=(50,40))
    if event =="Linen":
        window.close()
        window = sg.Window("Linen",layout_linen, margins=(50,40))        
    if event =="Sateen":
        window.close()
        window = sg.Window("Sateen",layout_sateen, margins=(50,40))  
    if event =="Silk":
        window.close()
        window = sg.Window("Silk",layout_silk, margins=(50,40))     
    if event =="Infused Silk":
        window.close()
        window = sg.Window("Infused Silk",layout_infusedsilk, margins=(50,40))
    if event =="Phoenixweave":
        window.close()
        window = sg.Window("Phoenixweave",layout_phoenixweave, margins=(50,40))        
    ### Weaving Branch ###    

    ### WOODWORKING BRANCH ###
    if event =="Woodworking":
        window.close() 
        window = sg.Window("Woodworking",layout_woodworking, margins=(50,40))
    if event =="Timber":
        window.close()
        window = sg.Window("Timber",layout_timber, margins=(50,40)) 
    if event =="Lumber":
        window.close()
        window = sg.Window("Lumber",layout_lumber, margins=(50,40))
    if event =="Wyrdwood Planks":
        window.close()
        window = sg.Window("Wyrdwood Planks",layout_wyrdwoodplanks, margins=(50,40))        
    if event =="Ironwood Planks":
        window.close()
        window = sg.Window("Ironwood Planks",layout_ironwoodplanks, margins=(50,40)) 
    if event =="Glittering Ebony":
        window.close()
        window = sg.Window("Glittering Ebony",layout_glitteringebony, margins=(50,40)) 
    ### WOODWORKING BRANCH ###
window.close()