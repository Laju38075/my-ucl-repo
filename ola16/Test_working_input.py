import PySimpleGUI as sg

layout = [
    [sg.Text('Enter something:'), sg.Input(key='iron_amount')],
    [sg.Text('Our output will go here', size=(30,1),key='-OUT-')],
    [sg.Button('OK'),sg.Button('Exit')]
]

window = sg.Window('Title',layout)

while True:
    event, values = window.read()
    if event == 'Exit' or event == sg.WIN_CLOSED:
        break
    if event == 'OK':
        height = int(values['iron_amount'])
        print(height)
window.close()